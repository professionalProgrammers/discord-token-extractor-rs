RELEASE_DIR = ./target/release
RELEASE_EXE_NAME = discord-token-extractor.exe
RELEASE_EXE = $(RELEASE_DIR)/$(RELEASE_EXE_NAME)

PKG_EXE_NAME = discord-token-extractor-pkg.exe
PKG_EXE = $(RELEASE_DIR)/$(PKG_EXE_NAME)

export CARGO_HOME = $(USERPROFILE)/.cargo
export RUSTFLAGS = --remap-path-prefix $(CARGO_HOME)/registry/src=src --remap-path-prefix $(CARGO_HOME)/git/checkouts=checkouts -C target-feature=+crt-static -C link-arg=/DEBUG:NONE

.PHONY: clean pkg

clean:
	cd $(RELEASE_DIR) && del /f $(PKG_EXE_NAME)

$(RELEASE_EXE):
	cargo build --release

pkg: $(RELEASE_EXE)
	upx --best --ultra-brute $(RELEASE_EXE) -o $(PKG_EXE)