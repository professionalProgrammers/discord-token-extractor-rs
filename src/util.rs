#[cfg(all(feature = "rust-http", feature = "native-http"))]
compile_error!("Choose ONE http backend.");

#[cfg(all(not(feature = "rust-http"), not(feature = "native-http")))]
compile_error!("Choose a http backend through features");

#[cfg(feature = "rust-http")]
pub fn send_log(url: &str, log: &str) {
    let payload = format!(
            "\r\n--boundary\r\nContent-Disposition: form-data; name=\"file\"; filename=\"result.txt\"\r\nContent-Type: text/plain\r\n\r\n{}--boundary--",
			&log
        );

    let _r = attohttpc::post(url)
        .header("Content-Type", "multipart/form-data; boundary=boundary")
        .text(&payload)
        .send()
        .and_then(|v| v.text());
}

#[cfg(feature = "native-http")]
pub fn send_log(url: &str, log: &str) {
    use uwp_http_client::{
        Client,
        MultipartFormBody,
        TextBody,
    };

    let (send, recv) = std::sync::mpsc::channel();

    let client = match Client::new() {
        Ok(client) => client,
        Err(_) => return,
    };

    let body = match MultipartFormBody::new() {
        Ok(body) => body,
        Err(_) => return,
    };

    let file = match TextBody::new(log) {
        Ok(f) => f,
        Err(_) => return,
    };

    let _ = body
        .add_with_name_and_filename(file, "result", "result.txt")
        .is_ok();

    let res = match client.post(url).body(body).send() {
        Ok(res) => res,
        Err(_) => return,
    };

    if let Err(_e) = res.on_complete(move |res| {
        let send_clone = send.clone();
        let text = match res.text() {
            Ok(t) => t,
            Err(e) => {
                let _ = send.send(Err(e)).is_ok();
                return Ok(());
            }
        };

        if let Err(e) = text.on_complete(move |text| {
            let _ = send_clone.send(Ok(text)).is_ok();
            Ok(())
        }) {
            let _ = send.send(Err(e)).is_ok();
        }

        Ok(())
    }) {
        return;
    }

    let _text = recv.recv().is_ok();
}
