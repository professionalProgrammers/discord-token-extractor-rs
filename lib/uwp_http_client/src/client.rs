use crate::{
    Error,
    RequestBuilder,
    Windows::{
        Foundation::Uri,
        Web::Http::{
            HttpClient,
            HttpMethod,
        },
    },
};

#[derive(Clone)]
pub struct Client {
    pub(crate) client: HttpClient,
}

impl Client {
    pub fn new() -> Result<Self, Error> {
        Ok(Client {
            client: HttpClient::new()?,
        })
    }

    pub fn get<U: AsRef<str>>(&self, u: U) -> RequestBuilder {
        RequestBuilder::new(self.clone(), HttpMethod::Get(), Uri::CreateUri(u.as_ref()))
    }

    pub fn post<U: AsRef<str>>(&self, u: U) -> RequestBuilder {
        RequestBuilder::new(self.clone(), HttpMethod::Post(), Uri::CreateUri(u.as_ref()))
    }
}
