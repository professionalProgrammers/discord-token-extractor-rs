#[allow(clippy::transmute_ptr_to_ptr)]
#[allow(clippy::type_complexity)]
#[allow(clippy::new_ret_no_self)]
pub mod bindings;
pub mod client;

use crate::Windows::{
    Foundation::{
        AsyncOperationWithProgressCompletedHandler,
        AsyncStatus,
        IAsyncOperationWithProgress,
        Uri,
    },
    Web::Http::{
        HttpMethod,
        HttpMultipartContent,
        HttpMultipartFormDataContent,
        HttpProgress,
        HttpRequestMessage,
        HttpRequestResult,
        HttpStringContent,
        IHttpContent,
    },
    Win32::System::SystemServices::E_POINTER,
};
pub use crate::{
    bindings::*,
    client::Client,
};
use std::marker::PhantomData;
use windows::HSTRING;

pub trait Body {
    fn get_content(&self) -> IHttpContent;
}

pub struct TextBody {
    body: HttpStringContent,
}

impl TextBody {
    pub fn new(s: &str) -> Result<Self, Error> {
        Ok(TextBody {
            body: HttpStringContent::CreateFromString(s)?,
        })
    }
}

impl Body for TextBody {
    fn get_content(&self) -> IHttpContent {
        self.body.clone().into()
    }
}

pub struct MultipartBody {
    body: HttpMultipartContent,
}

impl MultipartBody {
    pub fn new() -> Result<Self, Error> {
        Ok(MultipartBody {
            body: HttpMultipartContent::new()?,
        })
    }

    pub fn add<B: Body>(&self, b: B) -> Result<(), Error> {
        Ok(self.body.Add(b.get_content())?)
    }
}

impl Body for MultipartBody {
    fn get_content(&self) -> IHttpContent {
        self.body.clone().into()
    }
}

pub struct MultipartFormBody {
    body: HttpMultipartFormDataContent,
}

impl MultipartFormBody {
    pub fn new() -> Result<Self, Error> {
        Ok(MultipartFormBody {
            body: HttpMultipartFormDataContent::new()?,
        })
    }

    pub fn add<B: Body>(&self, b: B) -> Result<(), Error> {
        Ok(self.body.Add(b.get_content())?)
    }

    pub fn add_with_name<B: Body>(&self, b: B, name: &str) -> Result<(), Error> {
        Ok(self.body.AddWithName(b.get_content(), name)?)
    }

    pub fn add_with_name_and_filename<B: Body>(
        &self,
        b: B,
        name: &str,
        filename: &str,
    ) -> Result<(), Error> {
        Ok(self
            .body
            .AddWithNameAndFileName(b.get_content(), name, filename)?)
    }
}

impl Body for MultipartFormBody {
    fn get_content(&self) -> IHttpContent {
        self.body.clone().into()
    }
}

#[derive(Debug)]
pub enum Error {
    Windows(windows::Error),
    MissingAsyncAction,
}

impl From<windows::Error> for Error {
    fn from(e: windows::Error) -> Self {
        Error::Windows(e)
    }
}

pub struct RequestBuilder {
    request: Result<HttpRequestMessage, Error>,
    client: Client,
}

impl RequestBuilder {
    fn new(
        client: Client,
        method: Result<HttpMethod, windows::Error>,
        uri: Result<Uri, windows::Error>,
    ) -> Self {
        let request = match (method, uri) {
            (Ok(method), Ok(uri)) => HttpRequestMessage::Create(method, uri).map_err(Into::into),
            (Ok(_), Err(e)) => Err(e.into()),
            (Err(e), Ok(_)) => Err(e.into()),
            (Err(e), Err(_)) => Err(e.into()),
        };

        RequestBuilder { request, client }
    }

    pub fn body<B: Body>(mut self, body: B) -> Self {
        if let Ok(request) = self.request.as_ref() {
            if let Err(e) = request.SetContent(body.get_content()) {
                self.request = Err(e.into());
            }
        }

        self
    }

    pub fn header<S: AsRef<str>>(mut self, key: S, value: S) -> Self {
        if let Ok(request) = self.request.as_ref() {
            if let Err(e) = request
                .Headers()
                .and_then(|headers| headers.Append(key.as_ref(), value.as_ref()))
            {
                self.request = Err(Error::Windows(e));
            }
        }

        self
    }

    pub fn send(self) -> Result<AsyncOperation<Response, HttpRequestResult, HttpProgress>, Error> {
        let request = self.request?;

        Ok(AsyncOperation::new(
            self.client.client.TrySendRequestAsync(request)?,
        ))
    }
}

pub struct AsyncOperation<R, IR, IP>
where
    IR: windows::RuntimeType + 'static,
    IP: windows::RuntimeType + 'static,
{
    op: IAsyncOperationWithProgress<IR, IP>,
    _response_type: PhantomData<R>,
}

impl<R, IR, IP> AsyncOperation<R, IR, IP>
where
    IR: windows::RuntimeType + 'static,
    IP: windows::RuntimeType + 'static,
{
    fn new(op: IAsyncOperationWithProgress<IR, IP>) -> Self {
        AsyncOperation {
            op,
            _response_type: PhantomData,
        }
    }
}

impl<R, IR, IP> AsyncOperation<R, IR, IP>
where
    R: From<IR>,
    IR: windows::RuntimeType + 'static,
    IP: windows::RuntimeType + 'static,
{
    pub async fn future(self) -> Result<R, Error> {
        Ok(R::from(self.op.await?))
    }

    pub fn on_complete<F>(self, mut cb: F) -> Result<(), Error>
    where
        F: FnMut(R) -> Result<(), windows::Error> + 'static,
    {
        self.op
            .SetCompleted(AsyncOperationWithProgressCompletedHandler::new(
                move |op: &Option<IAsyncOperationWithProgress<IR, IP>>, _status: AsyncStatus| {
                    let op = op
                        .as_ref()
                        .ok_or_else(|| windows::Error::fast_error(E_POINTER))?;
                    let results = op.GetResults()?;
                    let r = R::from(results);
                    cb(r)
                },
            ))?;
        Ok(())
    }
}

pub struct Response {
    response: HttpRequestResult,
}

impl Response {
    pub fn text(self) -> Result<AsyncOperation<HSTRING, HSTRING, u64>, Error> {
        Ok(AsyncOperation::new(
            self.response
                .ResponseMessage()?
                .Content()?
                .ReadAsStringAsync()?,
        ))
    }
}

impl From<HttpRequestResult> for Response {
    fn from(response: HttpRequestResult) -> Self {
        Response { response }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    /*
    fn assert_send<T: Send>(_: T) {}

    #[test]
    fn static_assertions() {
        let client = Client::new().unwrap();
        let req = client.get("https://google.com");
        let fut = req.send().unwrap().future();

        // assert_send(fut); ComPtrs not send
    }

    #[tokio::test]
    async fn get_google_futures() {
        let client = Client::new().unwrap();
        let req = client.get("https://google.com");

        let res = req.send().unwrap().future().await.unwrap();
        let res = res.text().unwrap().future().await.unwrap();
        dbg!(res);
    }
    */

    #[test]
    fn get_google_callbacks() {
        let client = Client::new().unwrap();
        let req = client.get("https://google.com");
        let (send, recv) = std::sync::mpsc::channel();

        req.send()
            .unwrap()
            .on_complete(move |res| {
                let send_clone = send.clone();
                let cb = move |text| {
                    send_clone.send(Ok(text)).unwrap();
                    Ok(())
                };

                if let Err(e) = res.text().and_then(|res| res.on_complete(cb)) {
                    send.send(Err(e)).unwrap();
                }

                Ok(())
            })
            .unwrap();

        dbg!(recv.recv().unwrap().unwrap());
    }

    #[tokio::test]
    async fn post_httpbin_futures() {
        let client = Client::new().unwrap();
        let body = TextBody::new("\r\n--boundary\r\nContent-Disposition: form-data; name=\"file\"; filename=\"result.txt\"\r\nContent-Type: text/plain\r\n\r\ntest123abc--boundary--").unwrap();
        let res = client
            .post("https://httpbin.org/anything")
            .body(body)
            .send()
            .unwrap()
            .future()
            .await
            .unwrap()
            .text()
            .unwrap()
            .future()
            .await
            .unwrap();
        dbg!(res);
    }

    #[tokio::test]
    async fn post_multipart_httpbin_futures() {
        let client = Client::new().unwrap();
        let body = MultipartBody::new().unwrap();
        let body1 = TextBody::new("test").unwrap();
        body.add(body1).unwrap();

        let res = client
            .post("https://httpbin.org/anything")
            .body(body)
            .send()
            .unwrap()
            .future()
            .await
            .unwrap()
            .text()
            .unwrap()
            .future()
            .await
            .unwrap();
        dbg!(res);
    }

    #[tokio::test]
    async fn post_multipart_form_httpbin_futures() {
        let client = Client::new().unwrap();
        let body = MultipartFormBody::new().unwrap();
        let body1 = TextBody::new("test123abc").unwrap();
        body.add_with_name_and_filename(body1, "result", "result.txt")
            .unwrap();

        let res = client
            .post("https://httpbin.org/anything")
            .body(body)
            .send()
            .unwrap()
            .future()
            .await
            .unwrap()
            .text()
            .unwrap()
            .future()
            .await
            .unwrap();
        println!("{}", res);
    }
}
