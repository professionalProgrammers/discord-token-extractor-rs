fn main() {
    windows::build! (
        Windows::Web::Http::{
            HttpClient,
            HttpMethod,
            HttpMultipartContent,
            HttpMultipartFormDataContent,
            HttpProgress,
            HttpRequestMessage,
            HttpRequestResult,
            HttpStringContent,
            HttpResponseMessage,
            IHttpContent,
            Headers::HttpRequestHeaderCollection,
        },
        Windows::Foundation::{
            Uri,
            AsyncOperationWithProgressCompletedHandler,
            IAsyncOperationWithProgress,
        },
        Windows::Win32::System::SystemServices::E_POINTER,
    );
}
